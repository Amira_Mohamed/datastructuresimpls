package eg.edu.alexu.csd.ds.linkedList.csd17;

import java.util.Scanner;

public class App {
	static void readList (MyLinkedList l){
		boolean flag;
		Scanner scan = new Scanner(System.in);
		String s = scan.nextLine();
		String[] arr = s.split(";");
		for (int i=0;i<arr.length;i++){
			String str = arr[i].substring(1, arr[i].length()-1);
			String[] s1 = str.split(",");			
			int coff = Integer.parseInt(s1[0]);
			int exp = Integer.parseInt(s1[1]);
			if (coff!=0 && exp>=0){
				flag = false;
				for (int j=0;j<l.size();j++) {
					Pol Q = (Pol)l.get(j);
					if (Q.getExp()==exp) {l.set(j,new Pol(coff+Q.getCoff(),exp));flag=true;break;}
				}
				if (flag==false) l.add(new Pol(coff,exp));
			}
			if (exp<0) {
				System.out.println("Error: negative exponents aren't allowed in Polynomials");
				System.out.println("the Variable isn't set yet");
				return ; }
		}
		if (l.size()==0) {
			System.out.println("All Coefficients are zeros & the Variable isn't set yet");
			return ;	
		}
		System.out.println("Polynomial is set");
	}
	static void printPol (MyLinkedList l){
		for (int i=0;i<l.size();i++){
			Pol Q = (Pol) l.get(i);
			long c = Q.getCoff();
			long e = Q.getExp();
			if (i==0) {
				if (e==0) System.out.print(c);
				else {
					if (c==1) System.out.print("x^"+e);
					else if (c==-1) System.out.print("-x^"+e);
					else System.out.print(c+"x^"+e);}}
		
			else {
				if (c>0) System.out.print(" + ");
				else if (c<0) System.out.print(" - ");
				if (e==0) System.out.print(Math.abs(c));
				else {
				if (c==1||c==-1) System.out.print("x^"+e);
				else {
					System.out.print(Math.abs(c)+"x^"+e);}}
			}
		}
	}
	static void add_sub (MyLinkedList l1,MyLinkedList l2,MyLinkedList R,int o) {
		R.clear();
		boolean flag;
		for (int i=0;i<l1.size();i++){
			Pol Q = (Pol) l1.get(i);
			R.add(Q);
		}
		for (int j=0;j<l2.size();j++){
			Pol W = (Pol) l2.get(j);
			flag=false;
			for (int i=0;i<l1.size();i++){
				Pol Q = (Pol) l1.get(i);
				if (Q.getExp()==W.getExp()){
					flag=true;
					switch (o){ 
					case 1: {R.set(i,new Pol(Q.getCoff()+W.getCoff(),Q.getExp()));break;} 
					case 2: {R.set(i,new Pol(Q.getCoff()-W.getCoff(),Q.getExp()));break;}
					}
					break;
				}			
			}
			if (flag == false ){
				switch (o){
				case 1:{R.add(W);break;} // add
				case 2:{R.add(new Pol (-W.getCoff(),W.getExp()));}}} // sub
		}
	}
	static MyLinkedList isStored (char name,MyLinkedList A,MyLinkedList B,MyLinkedList C,MyLinkedList R){
		if (name=='A') {if (A.size()!=0) return A;}
		else if (name=='B') {if (B.size()!=0) return B;}	
		else if (name=='C') {if (C.size()!=0) return C;}
		else if (name=='R') {if (R.size()!=0) return R;}
		return null;
	}
	static void multiply (MyLinkedList l1,MyLinkedList l2,MyLinkedList R){
		R.clear();
		boolean flag;
		for (int i=0;i<l1.size();i++){
			Pol Q = (Pol) l1.get(i);
			for (int j=0;j<l2.size();j++){
				Pol W = (Pol) l2.get(j);
				Pol V = new Pol(Q.getCoff()*W.getCoff(),Q.getExp()+W.getExp());
				flag=false;
				for (int k=0;k<R.size();k++){
					Pol H =(Pol) R.get(k);
					if (H.getExp()==V.getExp()) {flag=true;R.set(k, new Pol(H.getCoff()+V.getCoff(),H.getExp()));break;}
				}
				if (flag==false) R.add(V);
			}
		}
	}
	static float EvaluateAtPoint (MyLinkedList l,float a){
		float result=0;
		int i=0;
		while (i<l.size()){
			Pol Q = (Pol) l.get(i++);
			result+=  Q.getCoff() * (float)Math.pow(a, Q.getExp());		 
		}
		return result;
	}
	public static void main(String[] args) {
		
		MyLinkedList A,B,C,R,t1,t2;
		A = new Slist();B = new Slist();C = new Slist();R = new Slist();
		int choose;
		char name;
		Scanner s = new Scanner(System.in);
		String[] menu = {"\n====================================================================","Please choose an action","-----------------------","1- Set a polynomial variable",
	    "2- Print the value of a polynomial variable","3- Add two polynomials",
	    "4- Subtract two polynomials", "5- Multiply two polynomials",
		"6- Evaluate a polynomial at some point", "7- Clear a polynomial variable","8- Exit\n"
		};
		
		while (true){
			for (int i=0;i<menu.length;i++){
				System.out.println(menu[i]);
			}
			choose=s.nextInt();
			//Set a polynomial variable
			if (choose==1) {
				System.out.println("Insert the variable name: A, B or C : ");
				name = s.next().charAt(0);
				if ((t1=isStored(name,A,B,C,R))!=null)System.out.println("Variable is set before Clear it First");
				else {
					System.out.println("Insert the polynomial terms in the form:");
					System.out.println("(coeff1, exponent1), (coeff2, exponent2), ..");
					if (name=='A') readList(A);
					else if (name=='B') readList(B);
					else if (name=='C') readList(C);
				}
			}
			// display the list // check is stored or not 
			else if (choose==2){
				System.out.println("Insert the variable name: A, B, C or R : ");
				name = s.next().charAt(0);
				if ((t1=isStored(name,A,B,C,R))!=null) printPol(t1);
				else System.out.println("Variable not set");
			}
			else if (choose==3||choose==4||choose==5){ char o1,o2;
			  int cnt=0;
			  if (isStored('A', A, B, C, R)!=null) cnt++;
			  if (isStored('B', A, B, C, R)!=null) cnt++;
			  if (isStored('C', A, B, C, R)!=null) cnt++;
			  if (cnt<2) System.out.println("no enough Variables are set");
			  else{ do {	
				System.out.println("Insert the first operand variable name: A, B or C : ");
				o1 = s.next().charAt(0); 
				t1=isStored(o1,A,B,C,R);
				if (t1==null) System.out.println("Variable not set");
			   }while (t1==null);
			  do {	
					System.out.println("Insert the second operand variable name: A, B or C :");
					o2 = s.next().charAt(0); 
					t2=isStored(o2,A,B,C,R);
					if (t2==null) System.out.println("Variable not set");
				   }while (t2==null);
			  switch (choose){
			  case 3:add_sub(t1,t2,R,1);break;
			  case 4:add_sub(t1,t2,R,2);break;
			  case 5:multiply(t1,t2,R);break;
			  }
			  System.out.println("Result: ");printPol(R);
			}}
			// check is stored or not 
			else if (choose==6){
				System.out.println("Insert the variable name: A, B, C or R :");
				name = s.next().charAt(0);
				if ((t1=isStored(name,A,B,C,R))!=null) {
					System.out.println("Insert the Point : ");
					float a = s.nextFloat();
					System.out.println("Result: "+EvaluateAtPoint(t1,a));
				}
				else System.out.println("Variable not set");
			}
			else if (choose==7){
				System.out.println("Insert the variable name: A, B or C : ");
				name = s.next().charAt(0);
				switch (name){
				case 'A': A.clear();break;
				case 'B': B.clear();break;
				case 'C': C.clear();break;
				}
				System.out.println("Variable IS cleared");
			}
			else return ;
		}	
	}
}
