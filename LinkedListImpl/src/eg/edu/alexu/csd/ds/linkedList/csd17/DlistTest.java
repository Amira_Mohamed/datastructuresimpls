package eg.edu.alexu.csd.ds.linkedList.csd17;

import static org.junit.Assert.*;
import org.junit.Assert;

import org.junit.Test;

public class DlistTest {

		@Test
		// check adding some elements correctly 
		public void testAddingCorrectly2 (){
			MyLinkedList list2 = new Dlist ();
			for (int i=0;i<20;i++){
				list2.add(i);
			}
			assertEquals(20,list2.size());
			for (int i=0;i<list2.size();i++){
				if ((int)list2.get(i) != i) Assert.fail();
			}
		}
		@Test
		//use add(index)  
		public void AddWithIndex3 (){
			MyLinkedList list2 = new Dlist ();
			for (int i=0;i<10;i++){
				list2.add(i);
			}
			assertEquals(10,list2.size());int size=10;
			list2.add(0, 'H');
			assertEquals(++size,list2.size());
			assertEquals('H',list2.get(0));
			list2.add(9, 'K');
			assertEquals(++size,list2.size());
			assertEquals('K',list2.get(9));
			list2.add(5, 'A');
			assertEquals(++size,list2.size());
			assertEquals('A',list2.get(5));
		}
		@Test
		// adding same element multiple times
		public void AddingSameElements4 (){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<10;i++){
				list1.add("Amira Mohammed");
			}
			assertEquals(10,list1.size());
			for (int i=0;i<10;i++){
				assertEquals("Amira Mohammed",(String)list1.get(i));
			}
			for (int j=10;j<15;j++){
				list1.add(j,"egypt");
			}
			assertEquals(15,list1.size());
			for (int j=10;j<15;j++){
				assertEquals("egypt",(String)list1.get(j));
			}
		}
		@Test
		// check invalid index of add 
		public void AddIndalidIndex1 (){
			MyLinkedList list1 = new Dlist ();
			list1.add(1);list1.add(2);list1.add(3);
			try {list1.add(-2,'W'); Assert.fail();} catch (RuntimeException e){}
		}
		@Test
		public void AddIndalidIndex2 (){
			MyLinkedList list1 = new Dlist ();
			list1.add('A');list1.add('B');list1.add('C');
			try {list1.add(5,'W'); Assert.fail();}catch (RuntimeException e){}
		}
		@Test
		// check invalid index of get
		public void GetIndalidIndex1 (){
			MyLinkedList list1 = new Dlist ();
			try {list1.get(0); Assert.fail();}catch (RuntimeException e){}
			list1.add(-5);list1.add(-6);list1.add(-7);
			try {list1.get(3); Assert.fail();}catch (RuntimeException e){}
		}
		@Test
		public void GetIndalidIndex2 (){
			MyLinkedList list1 = new Dlist ();
			list1.add(1);list1.add(2);list1.add(3);
			try {list1.get(-1); Assert.fail();} catch (RuntimeException e){}
		}
		@Test
		public void SetElementsCorrectly (){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<5;i++){
				list1.add(i);
			}
			for (int j=0;j<5;j++){
				list1.set(j, "Amira");
			}
			// size is the same 
			assertEquals(5,list1.size());
			for (int k=0;k<5;k++){
				assertEquals("Amira",list1.get(k));
			}
		}
		@Test
		public void setInvalidIndex1 (){
			MyLinkedList list1 = new Dlist ();
			// the object must be created before and so we can set in it
			try {list1.set(0,1); Assert.fail();}catch(RuntimeException e){}
			list1.add(1);list1.add(2);list1.add(3);
			try {list1.set(3,1); Assert.fail();}catch(RuntimeException e){}
		}
		@Test
		public void setInvalidIndex2 (){
			MyLinkedList list1 = new Dlist ();
			list1.add(10);list1.add(20);list1.add(30);
			try {list1.set(-1,1); Assert.fail();}catch(RuntimeException e){}
		}
		@Test
		// check contains method for indices  for existed elements
		public void checkContains1(){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<10;i++){
				list1.add(i);
			}
			for (int i=0;i<10;i++){
				assertEquals(true,list1.contains(i));
			}
		}
		@Test
		// contains with not existed elements 
		public void checkContains2(){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<10;i++){
				list1.add(i);
			}
			assertEquals(false,list1.contains(-1));
		}
		@Test
		// remove from last,middle,first
		public void removelast1(){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<2;i++){
				list1.add(i);
			}
			list1.remove(1);
			assertEquals(1,list1.size());
			try {list1.get(1);Assert.fail();}catch(RuntimeException e){}
		}
		@Test
		public void removefirst2(){
			MyLinkedList list1 = new Dlist ();
			list1.add('A');list1.add('B');list1.add('C');
			list1.remove(0);
			assertEquals(2,list1.size());
			if ((char)list1.get(0)!='B') Assert.fail();
		}
		@Test
		public void removeMiddle3(){
			MyLinkedList list1 = new Dlist ();
			list1.add('A');list1.add('B');list1.add('C');
			list1.remove(1);
			assertEquals(2,list1.size());
			if ((char)list1.get(1)!='C') Assert.fail();
		}
		@Test
		public void removeAllList (){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<5;i++){list1.add(i);}
			for (int j=0;j<5;j++){list1.remove(0);}
			assertEquals(0,list1.size());
			try {int t=(int)list1.get(1);Assert.fail();}catch (RuntimeException e){}
		}
		@Test // remove exceptions
		public void removeInvalidIndex (){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<5;i++){list1.add(i);}
			try {list1.remove(5);Assert.fail();}catch(RuntimeException e){}
			try {list1.remove(-1);Assert.fail();}catch(RuntimeException e){}
		}
		@Test // clear method 
		public void clearTest (){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<3;i++){list1.add(i);}
			list1.clear();
			assertEquals(0,list1.size());
			assertEquals(true,list1.isEmpty());
			try {int t=(int)list1.get(0);Assert.fail();}catch (RuntimeException e){}
		}
		@Test // sublist 
		public void sublist1 (){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<10;i++){list1.add(i);}
			MyLinkedList newlist = list1.sublist(2, 8);
			assertEquals(7,newlist.size());
			for (int j=0;j<7;j++){assertEquals(j+2,newlist.get(j));}
		}
		@Test // sublist invalid indexes
		public void sublistInvalidIndex (){
			MyLinkedList list1 = new Dlist ();
			for (int i=0;i<5;i++){list1.add(i);}
			MyLinkedList newlist;
			try {newlist=list1.sublist(3, 0);Assert.fail();}catch(RuntimeException e){} // from > less
			try {newlist=list1.sublist(-1, 3);Assert.fail();}catch(RuntimeException e){}// from is negative
			try {newlist=list1.sublist(3, 6);Assert.fail();}catch(RuntimeException e){}// to is bigger than size
		}
	}

