package eg.edu.alexu.csd.ds.linkedList.csd17;

public class Dlist implements MyLinkedList{
	private int size=0;  
	private DNode header=null,trailer=null; 
	
	boolean valid (int index){
		if (index>=0 && index<=size-1) return true;
		return false;
	}
	
	@Override
	public void add(int index, Object element) {
		if (index == size()) {add(element);return;}
		if (valid(index)){
		 DNode n,q,v; int i;
		 if (index <= (size-1)/2){i=0;q=header ;while (i<index) {q=q.getNext();i++;}}
		 else {i=size-1;q=trailer ;while (i>index) {q=q.getPrev();i--;}}
		 v=q.getPrev(); 
		 n = new DNode(element,q,v);
		 q.setPrev(n);
		 if (index==0) header=n; // or if (q==header) so v=null
		 else v.setNext(n);
		 size++;
		}
		else throw new  IllegalArgumentException("NOT VALID INDEX");
	}
	@Override
	public void add(Object element) {
		if (size==0) header=new DNode(element,null,null);
		else if (size==1) {trailer=new DNode(element,null,header);header.setNext(trailer);}
		else { 
			DNode n = new DNode(element,null,trailer);
			trailer.setNext(n);
			trailer = n;
		}
		size++;
	}
	@Override
	// return the element at the specified position
	public Object get(int index) {
		if (!valid(index)) throw new  IllegalArgumentException("NOT VALID INDEX");
		DNode q;int i;
		 if (index <= (size-1)/2){i=0;q=header ;while (i<index) {q=q.getNext();i++;}}
		 else {i=size-1;q=trailer ;while (i>index) {q=q.getPrev();i--;}}
		 return q.getElement();
	}

	@Override
	public void set(int index, Object element) {
		if (!valid(index)) throw new  IllegalArgumentException("NOT VALID INDEX");
		DNode q;int i;
		 if (index <= (size-1)/2){i=0;q=header ;while (i<index) {q=q.getNext();i++;}}
		 else {i=size-1;q=trailer ;while (i>index) {q=q.getPrev();i--;}}
		 q.setElement(element);
	}

	@Override
	public void clear() {
		size=0;
		header = null;
		trailer= null;
	}

	@Override
	public boolean isEmpty() {
		if (size==0) return true;
		return false;
	}

	@Override
	public void remove(int index) {
		if (valid(index)){
			DNode q,v,w;int i=0;
			if (index == 0){if(size()==1){clear();return;} else {v=header.getNext();v.setPrev(null);header=v;}}
			else if (index == size-1) {v=trailer.getPrev();v.setNext(null); if (size()>2) trailer=v;}
			else {
				 if (index <= (size-1)/2){i=0;q=header ;while (i<index) {q=q.getNext();i++;}}
				 else {i=size-1;q=trailer ;while (i>index) {q=q.getPrev();i--;}}
				v = q.getPrev();  w = q.getNext();
				v.setNext(w);  w.setPrev(v);
			}
			size--;
		} 
		else throw new  IllegalArgumentException("NOT VALID INDEX");
	}
	@Override
	public int size() { return size;}
	@Override
	public MyLinkedList sublist(int fromIndex, int toIndex) {
		if (!valid(fromIndex)||!valid(toIndex)||fromIndex>toIndex) 
			throw new IllegalArgumentException("NOT VALID INDICES");
		MyLinkedList newlist = new Slist();
		int i=0; DNode Q = header;
		while (i<fromIndex) {Q=Q.getNext();i++;}
		while (i<=toIndex) {
			newlist.add(Q.getElement());
			Q=Q.getNext();
			i++;
		}
		return newlist;
	}
	@Override
	public boolean contains(Object o) {
		DNode Q = header;
		while (Q!=null){
			if (Q.getElement()==o) return true;
			else Q=Q.getNext();
		}
		return false;
	}
}
