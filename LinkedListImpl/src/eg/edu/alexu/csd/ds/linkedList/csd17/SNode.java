package eg.edu.alexu.csd.ds.linkedList.csd17;

public class SNode {
	// single node is just data field and next pointer
	private Object o;
	private SNode next;
	// constructor to set nodes value and pointer 
	public SNode(Object o, SNode N) {
		this.o=o; next = N; 
	}
	public Object getElement() { return o; } 
	public SNode getNext() { return next; } 
	public void setElement (Object o) {this.o=o;}
	public void setNext (SNode n) {next=n;} 
}
