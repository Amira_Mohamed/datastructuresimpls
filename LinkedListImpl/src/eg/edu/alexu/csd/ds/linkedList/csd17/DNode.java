package eg.edu.alexu.csd.ds.linkedList.csd17;

public class DNode{
	private DNode pre,next;
	private Object o;
	public DNode(Object o, DNode N,DNode P) {
		this.o=o;
		pre=P;
		next=N;
	}
	public Object getElement() { return o; }
	public DNode getPrev() { return pre; }
	public DNode getNext() { return next; } 
	public void setElement(Object newElem) { o = newElem; } 
	public void setPrev(DNode newPrev) { pre = newPrev; } 
	public void setNext(DNode newNext) { next = newNext; }
	
}
