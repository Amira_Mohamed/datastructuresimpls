package eg.edu.alexu.csd.ds.queue.csd17;

public class LQueue implements MyQueue {
	private Slist l = new Slist();
	@Override
	// O(1)
	public void enqueue (Object element) {
		l.add(element);	// add at the last of the linked list 
	}
	// remove first element
	@Override
	// O(1)
	public Object dequeue() {
		if (l.isEmpty()) throw new IllegalStateException ("Queue iS Empty");
		Object t = l.get(0);
		l.remove(0);
		return t;
	}

	@Override
	// O(1)
	public boolean isEmpty() {
		if (l.isEmpty()) return true;
		return false;
	}

	@Override
	// O(1)
	public int size() {
		return l.size();
	}

}
