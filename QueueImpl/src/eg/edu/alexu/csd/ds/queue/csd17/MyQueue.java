package eg.edu.alexu.csd.ds.queue.csd17;

	public interface MyQueue {
		
		public void enqueue (Object element);
		
		public Object dequeue ();
		
		public boolean isEmpty ();
		
		public int size ();
		
	}
