package eg.edu.alexu.csd.ds.queue.csd17;

public class AQueue implements MyQueue{
	private int N; // size of the array 
	private	int size = 0 , front = 0;
	private	Object[] Q ;
	public AQueue (int n){
		N = n;
		Q = new Object [N];
	}
	
	@Override
	public void enqueue(Object element) {
		if (size==N) throw new IllegalStateException("Queue iS Full");
		Q[((size+front) % N)] = element;
		size++;
	}

	@Override
	public Object dequeue() {
		if (isEmpty()) throw new IllegalStateException ("Queue iS Empty");
		Object t = Q[front];
		front = ( front + 1 ) % N;
		size--;
		return t;
	}

	@Override
	public boolean isEmpty() {
		if (size==0) return true;
		return false;
	}

	@Override
	public int size() {
		return size;
	}

}
