package eg.edu.alexu.csd.ds.queue.csd17;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class AQueueTest {
	@Test
	// loop with size & isEmpty 
	// 1
	public void size_isEmpty (){
		int loop = 10;
		AQueue q = new AQueue(50);
		try {
			while (loop>0){

				for (int i=0;i<50;i++){
					q.enqueue(i);
				}
				assertEquals (q.size(),50);
				assertFalse(q.isEmpty());
			
				for (int j=0;j<50;j++){
					q.dequeue();
				}
				
				assertEquals (q.size(),0);
				assertTrue(q.isEmpty());
				
				loop--;
			}
		} catch(RuntimeException e){Assert.fail();}
	}
	@Test
	// Add same element 15 times & test size
	// 2
	public void sameElement (){
		AQueue q = new AQueue(15);
		try {
		for (int i=0;i<15;i++) 
			q.enqueue('a');
		assertEquals (q.size(),15);
		
			// it is need 15 times to dequeue it not 1 time
		for (int j=0;j<15;j++) 
			assertEquals ((char)q.dequeue(),'a');		
		assertTrue(q.isEmpty());
		}
		catch (RuntimeException e){Assert.fail();}
	}
	@Test
	// exception with dequeue
	// 3
	public void dequeue (){
		AQueue q = new AQueue(10);
		try {q.dequeue();Assert.fail();}
		catch (RuntimeException e){}
	}
	@Test
	//4
	// test values 
	// simply add some ordered elements and get them in the same order 
	public void checkOrder(){
		AQueue q = new AQueue(100);
		for (int i=0;i<100;i++) q.enqueue(i);
		for (int j=0;j<100;j++) assertEquals (q.dequeue(),j);
	}
	@Test
	//5
	// check for using all size 
	public void qu_de_si (){
		AQueue q = new AQueue(6);
		for (int i=0;i<6;i++) q.enqueue(i);
		q.dequeue();q.dequeue();q.dequeue();
		assertEquals (q.size(),3);
		q.enqueue(4);q.enqueue(5);q.enqueue(6);
		assertEquals (q.size(),6);
	}
	@Test
	//6
	// test queue full exception 
	public void fullQueue(){
		AQueue q = new AQueue(20);
		for (int i=0;i<20;i++) q.enqueue(i);
		try {
		q.enqueue("full");
		Assert.fail();
		}
		catch (RuntimeException e){}
	}
}
