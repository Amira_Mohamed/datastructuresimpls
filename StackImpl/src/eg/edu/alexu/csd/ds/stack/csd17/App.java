package eg.edu.alexu.csd.ds.stack.csd17;


import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Stack operations = new Stack(); 
		Stack result = new Stack() ;
		Stack matchParen = new Stack();
		double operand1,operand2,r=0;
		// x mean the beginning of expression
		// ( left paren
		// ) right paren
		// o operation
		// n number 
		char last = 'x'; // very important flag
		boolean nan = false;
		char c;
		StringBuffer postfix = new StringBuffer("");		
		Scanner scan = new Scanner (System.in);
		System.out.println("Enter the expression : "); 
		String str = scan.nextLine();
		String[] s = str.split("\\s+");
		for (int i=0;i<s.length;i++){
			if (s[i].length()==1){
				//1 //  (
				if (s[i].charAt(0)=='('){ 
					if (i==s.length-1||last==')'||last=='n') 
						{System.out.println("Not Valid Infix Expression");return;}
					else {
						matchParen.push('(');
						operations.push('(');
						last = '(';
					}
				}
				// 2  // )
				else if (s[i].charAt(0) ==')'){ 
					 if (matchParen.isEmpty()||!(last==')'||last=='n'))
					 	{System.out.println("Not Valid Infix Expression");return;}
					 else {
						 last = ')';
						 matchParen.pop();
						 c = (char)operations.pop();
						 while (c!='('){
							 postfix.append(c+" ");
							 if (nan) break;
							 operand1 = (double)result.pop();//1
							 operand2 = (double)result.pop();//2
							 switch (c){
							 case '+': {result.push(r=operand2+operand1);  break;}
							 case '-': {result.push(r=operand2-operand1);  break;}
							 case '*': {result.push(r=operand2*operand1);  break;}
							 case '/': {result.push(r=operand2/operand1);  break;}
							 case '%': {result.push(r=operand2%operand1);break;}
							 case '^': {
								 if ((operand2==(double)0||operand2==Double.POSITIVE_INFINITY||operand2==Double.NEGATIVE_INFINITY)&&operand1==0)
								 	{nan=true;result.push(Double.NaN);}
								 else result.push(r=Math.pow(operand2,operand1));
								 break; 
							 }
							 }if (r==Double.NaN) nan=true;
							 c = (char)operations.pop();
						 }
					 } 
				}
				// 3 //  + , -
				else if (s[i].charAt(0)=='+'||s[i].charAt(0)=='-'){
					if (i==s.length-1||!(last==')'||last=='n'))
						{System.out.println("Not Valid Infix Expression");return;} 
					else {
						last = 'o';
						while (!operations.isEmpty()){
							c = (char)operations.pop();
							if (c=='(') {operations.push(c);break;}
							else {
								postfix.append(c+" ");
								if (!nan){
								operand1 = (double)result.pop();//new
								operand2 = (double)result.pop();//old
								 switch (c){
								 case '+': {result.push(r=operand2+operand1);  break;}
								 case '-': {result.push(r=operand2-operand1);  break;}
								 case '*': {result.push(r=operand2*operand1);  break;}
								 case '/': {result.push(r=operand2/operand1);  break;}
								 case '%': {result.push(r=operand2%operand1);break;}
								 case '^': {
									 if ((operand2==(double)0||operand2==Double.POSITIVE_INFINITY||operand2==Double.NEGATIVE_INFINITY)&&operand1==0)
									 	{nan=true;result.push(Double.NaN);}
									 else result.push(r=Math.pow(operand2,operand1));
									 break; 
								 }
								 }if (r==Double.NaN) nan=true;
								}
							}
						}
						operations.push(s[i].charAt(0));
					}
				}
				// 4 // * , / , % 
				else if (s[i].charAt(0)=='*'||s[i].charAt(0)=='/'|| s[i].charAt(0)== '%'){
					if (i==s.length-1||!(last==')'||last=='n'))
						{System.out.println("Not Valid Infix Expression");return;}
					else {
						last = 'o';
						while (!operations.isEmpty()){
							c = (char)operations.pop();
							if (c=='('||c=='+'||c=='-') {operations.push(c);break;}
							else {
								postfix.append(c+" ");
								if (!nan){
								operand1 = (double)result.pop();
								operand2 = (double)result.pop();
								 switch (c){
								 case '*': {result.push(r=operand2*operand1);  break;}
								 case '/': {result.push(r=operand2/operand1);  break;}
								 case '%': {result.push(r=operand2%operand1);break;}
								 case '^': {
									 if ((operand2==(double)0||operand2==Double.POSITIVE_INFINITY||operand2==Double.NEGATIVE_INFINITY)&&operand1==0)
									 	{nan=true;result.push(Double.NaN);}
									 else result.push(r=Math.pow(operand2,operand1));
									 break; 
								 }
								 }if (r==Double.NaN) nan=true;
								}
							}
						}
						operations.push(s[i].charAt(0));
					}	
				}
				// 5 // power 
				else if (s[i].charAt(0)=='^'){
					if (i==s.length-1||!(last==')'||last=='n')) {
						System.out.println("Not Valid Infix Expression");return;
					}
					else {
						last = 'o';operations.push(s[i].charAt(0));
					}
				}
				// 6 // digit
				else if (Character.isDigit(s[i].charAt(0))){
					if (last=='n'||last==')'){System.out.println("Not Valid Infix Expression");return;}
					else{
						last = 'n';
						postfix.append(s[i]+" ");
						if (!nan)
							{result.push(Double.parseDouble(s[i]));}
					}
				}
				// 7 // else not valid
				else {System.out.println("Not Valid Infix Expression");return;}
			}
			else { // length != 1
				if (last=='n'||last==')'){System.out.println("Not Valid Infix Expression");return;}
				else {
					// test all digits of the number 
					for (int j=0;j<s[i].length();j++){
						if (!Character.isDigit(s[i].charAt(j))) {
							System.out.println("Not Valid Infix Expression");return;
						}
					}
					last = 'n';
					postfix.append(s[i]+" ");
					if (!nan)
						result.push(Double.parseDouble(s[i]));
				}
			}
		}
		// complete the rest of the operations after reaching end of expression
		while (!operations.isEmpty()){
			c = (char)operations.pop();
			postfix.append(c+" ");
			if (!nan){
			operand1 = (double)result.pop();
			operand2 = (double)result.pop();
			 switch (c){
			 case '+': {result.push(r=operand2+operand1);  break;}
			 case '-': {result.push(r=operand2-operand1);  break;}
			 case '*': {result.push(r=operand2*operand1);  break;}
			 case '/': {result.push(r=operand2/operand1);  break;}
			 case '%': {result.push(r=operand2%operand1);break;}
			 case '^': {
				 if ((operand2==(double)0||operand2==Double.POSITIVE_INFINITY||operand2==Double.NEGATIVE_INFINITY)&&operand1==0)
				 	{nan=true;result.push(Double.NaN);}
				 else result.push(r=Math.pow(operand2,operand1));
				 break; 
			 }
			 }if (r==Double.NaN) nan=true;
			}
		}
		
		System.out.println("Expression in Posfix: "+postfix.toString());
		System.out.println("Result: "+(float)(double)result.pop());
	}
}

