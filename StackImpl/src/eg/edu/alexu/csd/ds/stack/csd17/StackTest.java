package eg.edu.alexu.csd.ds.stack.csd17;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.EmptyStackException;

import org.junit.Test;
import org.junit.Assert;
public class StackTest {
	
	Stack s1 = new Stack ();
	char[] a = {'A','B','C','D','E','F','G'};	
	@Test
	// Empty stack	
	// add
	public void emptyStack1 () {
		try {
			s1.add(-1, "Amira");
			Assert.fail ();
		}
		catch (RuntimeException e){}
	}
	@Test
	public void emptyStack2 (){
		try {
			s1.add(1, "Mohammed");
			Assert.fail ();
		}
		catch (RuntimeException e){}
	}
	@Test
	public void emptyStack3 (){
		try {
			Object o = (int) 5;
			o = s1.peek();
			// if not exception so it should return null else 
			if (o!=null) Assert.fail ("stack is empty & return value of peek isn't suitable");
		}
		catch (RuntimeException e){//test succeeded
			}
	}
	@Test
	//pop
	public void emptyStack4(){
		try {
			Object o = (int) 5;
			o = s1.pop();
			// if not exception so it should return null else 
			if (o!=null) Assert.fail ();
		}
		catch (RuntimeException e){}
	}
	@Test
	// size && isEmpty 
	public void emptyStack5(){
		assertEquals (0,s1.size());
		assertEquals (true,s1.isEmpty());
	}
	
	@Test
	// stack with elements
	public void SizePush6 (){
		for (int i=0;i<a.length;i++){
			s1.push(a[i]);
		}
		assertEquals (a.length,s1.size());
		assertEquals (false,s1.isEmpty());
	}
	
	@Test
	// repeated calls for peak
	public void repeatedPeak7 (){
		for (int i=0;i<a.length;i++){
			s1.push(a[i]);
		}
		for (int j=0;j<a.length;j++){
			assertEquals('G',(char)s1.peek());
		}
		assertEquals (a.length,s1.size());
		assertEquals(false,s1.isEmpty());
	}
	
	@Test
	// peak with pop
	public void PeakPop8 (){
		for (int i=0;i<a.length;i++){
			s1.push(a[i]);
		}
		int size=a.length;
		for (int j=a.length-1;j>=0;j--){
			assertEquals(a[j],s1.peek());
			assertEquals(size--,s1.size());
			s1.pop();
		}
	}

	Stack s2 = new Stack ();
	int[] b = {10,20,30,40,50,60};
	@Test 
	public void PushPop9 (){
		int size=b.length;
		for (int i=0;i<b.length;i++){
			s2.push(b[i]);
		}
		for (int j=b.length-1;j>=0;j--){
			assertEquals(b[j],s2.pop());
			assertEquals(--size,s2.size());
		}
		assertEquals(true,s2.isEmpty());
	}
	Stack s3 = new Stack ();
	int flag=0;
	@Test
	public void Add10 (){
		s3.add(0, 'X');s3.add(0, 'Y');s3.add(0, 'Z');
		assertEquals(3,s3.size());
		char[] t1 = {'Z','Y','X'}; // pop mean get (0)
		char[] t2 = {'X','Y','Z'}; // pop mean get from last
		char[] t = new char[3];
		for (int k=0;k<3;k++) t[k] = (char) s3.pop();
		if (Arrays.equals(t, t1)) flag = 1;
		else if (Arrays.equals(t, t2))	 flag = 2;
		else Assert.fail();
	}
	@Test
	public void PushPopAdd11 (){
		if (flag!=0){
			s3.push('X');s3.push('Y');s3.push('Z');
			s3.add(2,'A');
			assertEquals(4,s3.size());
			char[] t = new char[4];
			for (int k=0;k<4;k++){
				t[k] = (char) s3.pop();
			}
			char[] t1 = {'Z','Y','A','X'}; // pop from first
			char[] t2 = {'Z','A','Y','X'}; // pop from last
			if (flag==1) {Assert.assertArrayEquals(t1, t);}
			else if (flag==2) {Assert.assertArrayEquals(t2, t);}
		}
	}
}

	
	

