package eg.edu.alexu.csd.ds.stack.csd17;

	public class Slist implements MyLinkedList{
		
		private SNode head=null,tail=null; 
		private int size=0; 
		
		boolean valid (int index){
			if (index>=0 && index<=size-1) return true;
			return false;
		}
		
		@Override
		// insert an element in specified position 
		public void add(int index,Object element){
			if (index==size()) {add(element);return;} // add last 
			if (valid(index)) {				
				if (index==0) {
					SNode New = new SNode(element,head); // add first
					head = New;
				}
				else {
					SNode Q=head;
					int i=0;
					while (i<index-1) {Q=Q.getNext();i++;}					
					SNode New = new SNode(element,Q.getNext());
					Q.setNext(New);
				}
				size++;
			}
			else throw new IllegalArgumentException("NOT VALID INDEX");	
		}
		@Override
		// insert an element in the end of the list
		public void add(Object element) {
			SNode n = new SNode(element,null);
			if (size==0) head = n;
			else if (size==1) {tail = n;head.setNext(tail);}
			else { 
				tail.setNext(n);
				tail = n;
			}
			size++;
		}
		@Override
		// return the element at the specified position 
		public Object get(int index) {
			if (!valid(index)) throw new IllegalArgumentException("NOT VALID INDEX");
			int i=0; SNode Q = head; 
			while (i<index) {Q=Q.getNext();i++;}
			return Q.getElement();
		}	
		@Override
		// replace the element at the specified position in the list with the specified element
		public void set(int index, Object element) {
			if (valid(index)) {
				int i=0; SNode Q = head;
				while (i<index) {Q=Q.getNext();i++;}
				Q.setElement(element);
			}
			else throw new IllegalArgumentException("NOT VALID INDEX");
		}
		@Override
		// remove all elements from the list
		public void clear() {
			head = null;
			tail=null;
			size=0;	
		}
		@Override
		public boolean isEmpty() {
			if (size==0) return true;
			return false;
		}
		@Override
		// remove the element at the specified position from the list 
		public void remove(int index) {
			if (valid(index)) {
				SNode Q = head;
				if (index==0) {
					if (size()==1) {clear();return;}
					head= Q.getNext();
					Q.setNext(null);
				}
				else {
					int i=0;  
					while (i<index-1) {Q=Q.getNext();i++;}
					if (index==size-1) {Q.setNext(null);tail=Q;}
					else { SNode V = Q.getNext(); // remove V
						   Q.setNext(V.getNext());
						   V.setNext(null); }
				}
				size--;
			}
			else throw new IllegalArgumentException("NOT VALID INDEX");
		}
		@Override
		// return the number of elements in the list
		public int size() {
			return size;
		}
		@Override
		public MyLinkedList sublist(int fromIndex, int toIndex) {
			if (!valid(fromIndex)||!valid(toIndex)||fromIndex>toIndex) 
				throw new IllegalArgumentException("NOT VALID INDICES");
			MyLinkedList newlist = new Slist();
			int i=0; SNode Q = head;
			while (i<fromIndex) {Q=Q.getNext();i++;}
			while (i<=toIndex) {
				// copy 
				newlist.add(Q.getElement());
				Q=Q.getNext();
				i++;
			}
			return newlist;
		}
		@Override
		public boolean contains(Object o) {
			SNode Q = head;
			while (Q!=null){
				if (Q.getElement()==o) return true;
				else Q=Q.getNext();
			}
			return false;
		}
	}
