package eg.edu.alexu.csd.ds.stack.csd17;


	public interface MyLinkedList {
		
		// insert an element in specified position 
		public  void add (int index,Object element);
		
		// insert an element in the end of the list
		public void add (Object element);
		
		// return the element at the specified position 
		public Object get (int index);
		
		// replace the element at the specified position in the list with the specified element
		public void set (int index,Object element);
		
		// remove all elements from the list 
		public void clear();
		
		// return true if this list contains no elements
		public boolean isEmpty();
		
		// remove the element at the specified position from the list 
		public void remove (int index);
		
		// return the number of elements in the list
		public int size();
		
		//Returns a view of the portion of this list between the specified
		//fromIndex and toIndex, inclusively.
		public MyLinkedList sublist (int fromIndex,int toIndex);
			
		// Returns true if this list contains an element with the same value
		// as the specified element.
		public boolean contains (Object o);
	}
